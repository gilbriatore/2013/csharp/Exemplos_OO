﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exemplos_OO
{
    class Nota
    {
        public string aluno;
        public float b1;
        public float b2;
        public float b3;
        public float b4;

        public float calcularMedia()
        {
            return (b1 + b2 + b3 + b4) / 4;
        }

        public string verificarSituacao(float media)
        {
            if (media >= 7)
            {
                return "Aprovado";
            }
            else
            {
                if (media < 2.5)
                {
                    return "Reprovado";
                }
                else
                {
                    return "Recuperação";
                }
            }
        }
    }
}
