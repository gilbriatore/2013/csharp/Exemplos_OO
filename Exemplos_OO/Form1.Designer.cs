﻿namespace Exemplos_OO
{
    partial class frmExemplo1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtAluno = new System.Windows.Forms.TextBox();
            this.txtB1 = new System.Windows.Forms.TextBox();
            this.txtB2 = new System.Windows.Forms.TextBox();
            this.txtB3 = new System.Windows.Forms.TextBox();
            this.txtB4 = new System.Windows.Forms.TextBox();
            this.txtMedia = new System.Windows.Forms.TextBox();
            this.txtSituacao = new System.Windows.Forms.TextBox();
            this.btnGerar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtAluno
            // 
            this.txtAluno.Location = new System.Drawing.Point(16, 16);
            this.txtAluno.Name = "txtAluno";
            this.txtAluno.Size = new System.Drawing.Size(236, 20);
            this.txtAluno.TabIndex = 0;
            // 
            // txtB1
            // 
            this.txtB1.Location = new System.Drawing.Point(16, 44);
            this.txtB1.Name = "txtB1";
            this.txtB1.Size = new System.Drawing.Size(84, 20);
            this.txtB1.TabIndex = 1;
            // 
            // txtB2
            // 
            this.txtB2.Location = new System.Drawing.Point(16, 72);
            this.txtB2.Name = "txtB2";
            this.txtB2.Size = new System.Drawing.Size(84, 20);
            this.txtB2.TabIndex = 2;
            // 
            // txtB3
            // 
            this.txtB3.Location = new System.Drawing.Point(16, 100);
            this.txtB3.Name = "txtB3";
            this.txtB3.Size = new System.Drawing.Size(84, 20);
            this.txtB3.TabIndex = 3;
            // 
            // txtB4
            // 
            this.txtB4.Location = new System.Drawing.Point(16, 128);
            this.txtB4.Name = "txtB4";
            this.txtB4.Size = new System.Drawing.Size(84, 20);
            this.txtB4.TabIndex = 4;
            // 
            // txtMedia
            // 
            this.txtMedia.Location = new System.Drawing.Point(16, 168);
            this.txtMedia.Name = "txtMedia";
            this.txtMedia.Size = new System.Drawing.Size(84, 20);
            this.txtMedia.TabIndex = 5;
            // 
            // txtSituacao
            // 
            this.txtSituacao.Location = new System.Drawing.Point(16, 200);
            this.txtSituacao.Name = "txtSituacao";
            this.txtSituacao.Size = new System.Drawing.Size(164, 20);
            this.txtSituacao.TabIndex = 6;
            // 
            // btnGerar
            // 
            this.btnGerar.Location = new System.Drawing.Point(144, 60);
            this.btnGerar.Name = "btnGerar";
            this.btnGerar.Size = new System.Drawing.Size(75, 23);
            this.btnGerar.TabIndex = 7;
            this.btnGerar.Text = "&Gerar dados";
            this.btnGerar.UseVisualStyleBackColor = true;
            this.btnGerar.Click += new System.EventHandler(this.btnGerar_Click);
            // 
            // frmExemplo1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 253);
            this.Controls.Add(this.btnGerar);
            this.Controls.Add(this.txtSituacao);
            this.Controls.Add(this.txtMedia);
            this.Controls.Add(this.txtB4);
            this.Controls.Add(this.txtB3);
            this.Controls.Add(this.txtB2);
            this.Controls.Add(this.txtB1);
            this.Controls.Add(this.txtAluno);
            this.Name = "frmExemplo1";
            this.Text = "Exemplo 1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtAluno;
        private System.Windows.Forms.TextBox txtB1;
        private System.Windows.Forms.TextBox txtB2;
        private System.Windows.Forms.TextBox txtB3;
        private System.Windows.Forms.TextBox txtB4;
        private System.Windows.Forms.TextBox txtMedia;
        private System.Windows.Forms.TextBox txtSituacao;
        private System.Windows.Forms.Button btnGerar;
    }
}

