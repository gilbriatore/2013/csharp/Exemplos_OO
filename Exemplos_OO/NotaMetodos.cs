﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exemplos_OO
{
    class NotaMetodos
    {
        public static float calcularMedia(Nota5 nota)
        {
            return (nota.b1 + nota.b2 + nota.b3 + nota.b4) / 4;
        }

        public static string verificarSituacao(Nota5 nota)
        {
            float media = calcularMedia(nota);
            if (media >= 7)
            {
                return "Aprovado";
            }
            else
            {
                if (media < 2.5)
                {
                    return "Reprovado";
                }
                else
                {
                    return "Recuperação";
                }
            }
        }
    }
}
