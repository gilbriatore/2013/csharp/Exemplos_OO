﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exemplos_OO
{
    class Nota1
    {
        private string aluno;
        private float b1;
        private float b2;
        private float b3;
        private float b4;

        public string Aluno
        {
            get { return aluno; }
            set { aluno = value; }
        }

        public float B1
        {
            get { return b1; }
            set { b1 = value; }
        }

        public float B2
        {
            get { return b2; }
            set { b2 = value; }
        }

        public float B3
        {
            get { return b3; }
            set { b3 = value; }
        }

        public float B4
        {
            get { return b4; }
            set { b4 = value; }
        }

        public float calcularMedia()
        {
            return (b1 + b2 + b3 + b4) / 4;
        }

        public string verificarSituacao(float media)
        {
            if (media >= 7)
            {
                return "Aprovado";
            }
            else
            {
                if (media < 2.5)
                {
                    return "Reprovado";
                }
                else
                {
                    return "Recuperação";
                }
            }
        }
    }
}
