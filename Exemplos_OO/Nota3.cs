﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exemplos_OO
{
    class Nota3
    {
        public string aluno { set; get; }
        public float b1 { set; get; }
        public float b2 { set; get; }
        public float b3 { set; get; }
        public float b4 { set; get; }

        public Nota3() { }
        
        public Nota3(string aluno)
        {
            this.aluno = aluno;
        }

        public Nota3(string aluno, float b1, float b2, float b3, float b4)
        {
            this.aluno = aluno;
            this.b1 = b1;
            this.b2 = b2;
            this.b3 = b3;
            this.b4 = b4;
        }

        public float calcularMedia()
        {
            return (b1 + b2 + b3 + b4) / 4;
        }

        public string verificarSituacao(float media)
        {
            if (media >= 7)
            {
                return "Aprovado";
            }
            else
            {
                if (media < 2.5)
                {
                    return "Reprovado";
                }
                else
                {
                    return "Recuperação";
                }
            }
        }
    }
}
