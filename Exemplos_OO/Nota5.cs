﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exemplos_OO
{
    class Nota5
    {
        public Aluno aluno { set; get; }
        public float b1 { set; get; }
        public float b2 { set; get; }
        public float b3 { set; get; }
        public float b4 { set; get; }

        public Nota5() { }
        
        public Nota5(Aluno aluno)
        {
            this.aluno = aluno;
        }

        public Nota5(Aluno aluno, float b1, float b2, float b3, float b4)
        {
            this.aluno = aluno;
            this.b1 = b1;
            this.b2 = b2;
            this.b3 = b3;
            this.b4 = b4;
        }
    }
}
