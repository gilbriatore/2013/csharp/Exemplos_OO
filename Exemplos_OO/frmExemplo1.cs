﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Exemplos_OO
{
    public partial class frmExemplo1 : Form
    {
        public frmExemplo1()
        {
            InitializeComponent();
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            float media;
            Nota1 nt = new Nota1();
            nt.Aluno = txtAluno.Text;
            nt.B1 = float.Parse(txtB1.Text);
            nt.B2 = float.Parse(txtB2.Text);
            nt.B3 = float.Parse(txtB3.Text);
            nt.B4 = float.Parse(txtB4.Text);
            MessageBox.Show("Aluno: " + nt.Aluno + "\n" +
                            "Nota 1: " + nt.B1 + "\n" +
                            "Nota 2: " + nt.B2 + "\n" +
                            "Nota 3: " + nt.B3 + "\n" +
                            "Nota 4: " + nt.B4);
            media = nt.calcularMedia();
            txtMedia.Text = media.ToString();
            txtSituacao.Text = nt.verificarSituacao(media);
        }

        private void txtSituacao_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMedia_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAluno_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
