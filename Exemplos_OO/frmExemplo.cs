﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Exemplos_OO
{
    public partial class frmExemplo : Form
    {
        public frmExemplo()
        {
            InitializeComponent();
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            float media;
            Nota nt;
            nt = new Nota();
            nt.aluno = txtAluno.Text;
            nt.b1 = float.Parse(txtB1.Text);
            nt.b2 = float.Parse(txtB2.Text);
            nt.b3 = float.Parse(txtB3.Text);
            nt.b4 = float.Parse(txtB4.Text);
            MessageBox.Show("Aluno: " + nt.aluno + "\n" +
                            "Nota 1: " + nt.b1 + "\n" +
                            "Nota 2: " + nt.b2 + "\n" +
                            "Nota 3: " + nt.b3 + "\n" +
                            "Nota 4: " + nt.b4);
            media = nt.calcularMedia();
            txtMedia.Text = media.ToString();
            txtSituacao.Text = nt.verificarSituacao(media);
        }

        private void txtB1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMedia_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtSituacao_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAluno_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
