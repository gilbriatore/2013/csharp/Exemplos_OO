﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exemplos_OO
{
    class Nota2
    {
        public string aluno { set; get; }
        public float b1 { set; get; }
        public float b2 { set; get; }
        public float b3 { set; get; }
        public float b4 { set; get; }

        public float calcularMedia()
        {
            return (b1 + b2 + b3 + b4) / 4;
        }

        public string verificarSituacao(float media)
        {
            if (media >= 7)
            {
                return "Aprovado";
            }
            else
            {
                if (media < 2.5)
                {
                    return "Reprovado";
                }
                else
                {
                    return "Recuperação";
                }
            }
        }
    }
}
