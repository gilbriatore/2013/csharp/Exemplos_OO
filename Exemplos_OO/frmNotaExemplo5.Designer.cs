﻿namespace Exemplos_OO
{
    partial class frmNotaExemplo5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCpf = new System.Windows.Forms.TextBox();
            this.btnGerar = new System.Windows.Forms.Button();
            this.txtSituacao = new System.Windows.Forms.TextBox();
            this.txtMedia = new System.Windows.Forms.TextBox();
            this.txtB4 = new System.Windows.Forms.TextBox();
            this.txtB3 = new System.Windows.Forms.TextBox();
            this.txtB2 = new System.Windows.Forms.TextBox();
            this.txtB1 = new System.Windows.Forms.TextBox();
            this.txtAluno = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtCpf
            // 
            this.txtCpf.Location = new System.Drawing.Point(24, 43);
            this.txtCpf.Name = "txtCpf";
            this.txtCpf.Size = new System.Drawing.Size(136, 20);
            this.txtCpf.TabIndex = 41;
            // 
            // btnGerar
            // 
            this.btnGerar.Location = new System.Drawing.Point(156, 107);
            this.btnGerar.Name = "btnGerar";
            this.btnGerar.Size = new System.Drawing.Size(75, 23);
            this.btnGerar.TabIndex = 48;
            this.btnGerar.Text = "&Gerar dados";
            this.btnGerar.UseVisualStyleBackColor = true;
            this.btnGerar.Click += new System.EventHandler(this.btnGerar_Click);
            // 
            // txtSituacao
            // 
            this.txtSituacao.Location = new System.Drawing.Point(24, 227);
            this.txtSituacao.Name = "txtSituacao";
            this.txtSituacao.Size = new System.Drawing.Size(164, 20);
            this.txtSituacao.TabIndex = 47;
            // 
            // txtMedia
            // 
            this.txtMedia.Location = new System.Drawing.Point(24, 195);
            this.txtMedia.Name = "txtMedia";
            this.txtMedia.Size = new System.Drawing.Size(84, 20);
            this.txtMedia.TabIndex = 46;
            // 
            // txtB4
            // 
            this.txtB4.Location = new System.Drawing.Point(24, 155);
            this.txtB4.Name = "txtB4";
            this.txtB4.Size = new System.Drawing.Size(84, 20);
            this.txtB4.TabIndex = 45;
            // 
            // txtB3
            // 
            this.txtB3.Location = new System.Drawing.Point(24, 127);
            this.txtB3.Name = "txtB3";
            this.txtB3.Size = new System.Drawing.Size(84, 20);
            this.txtB3.TabIndex = 44;
            // 
            // txtB2
            // 
            this.txtB2.Location = new System.Drawing.Point(24, 99);
            this.txtB2.Name = "txtB2";
            this.txtB2.Size = new System.Drawing.Size(84, 20);
            this.txtB2.TabIndex = 43;
            // 
            // txtB1
            // 
            this.txtB1.Location = new System.Drawing.Point(24, 71);
            this.txtB1.Name = "txtB1";
            this.txtB1.Size = new System.Drawing.Size(84, 20);
            this.txtB1.TabIndex = 42;
            // 
            // txtAluno
            // 
            this.txtAluno.Location = new System.Drawing.Point(24, 15);
            this.txtAluno.Name = "txtAluno";
            this.txtAluno.Size = new System.Drawing.Size(236, 20);
            this.txtAluno.TabIndex = 40;
            // 
            // frmNotaExemplo5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.txtCpf);
            this.Controls.Add(this.btnGerar);
            this.Controls.Add(this.txtSituacao);
            this.Controls.Add(this.txtMedia);
            this.Controls.Add(this.txtB4);
            this.Controls.Add(this.txtB3);
            this.Controls.Add(this.txtB2);
            this.Controls.Add(this.txtB1);
            this.Controls.Add(this.txtAluno);
            this.Name = "frmNotaExemplo5";
            this.Text = "frmNotaExemplo5";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCpf;
        private System.Windows.Forms.Button btnGerar;
        private System.Windows.Forms.TextBox txtSituacao;
        private System.Windows.Forms.TextBox txtMedia;
        private System.Windows.Forms.TextBox txtB4;
        private System.Windows.Forms.TextBox txtB3;
        private System.Windows.Forms.TextBox txtB2;
        private System.Windows.Forms.TextBox txtB1;
        private System.Windows.Forms.TextBox txtAluno;
    }
}