﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exemplos_OO
{
    class Aluno
    {
        public string nome { set; get; }
        public string cpf { set; get; }

        public Aluno() { }
        public Aluno(string nome, string cpf)
        {
            this.nome = nome;
            this.cpf = cpf;
        }
    }
}
