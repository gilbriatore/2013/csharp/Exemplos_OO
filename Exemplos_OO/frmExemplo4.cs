﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Exemplos_OO
{
    public partial class frmExemplo4 : Form
    {
        public frmExemplo4()
        {
            InitializeComponent();
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            Aluno aluno = new Aluno();
            aluno.nome = txtAluno.Text;
            aluno.cpf = txtCpf.Text;
            Nota4 nota = new Nota4();
            nota.aluno = aluno;
            nota.b1 = float.Parse(txtB1.Text);
            nota.b2 = float.Parse(txtB2.Text);
            nota.b3 = float.Parse(txtB3.Text);
            nota.b4 = float.Parse(txtB4.Text);
            MessageBox.Show("Aluno: " + nota.aluno + "\n" +
                            "Nota 1: " + nota.b1 + "\n" +
                            "Nota 2: " + nota.b2 + "\n" +
                            "Nota 3: " + nota.b3 + "\n" +
                            "Nota 4: " + nota.b4);
        }

        private void txtSituacao_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMedia_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAluno_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCpf_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
