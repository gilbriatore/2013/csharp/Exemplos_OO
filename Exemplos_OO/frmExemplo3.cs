﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Exemplos_OO
{
    public partial class frmExemplo3 : Form
    {
        public frmExemplo3()
        {
            InitializeComponent();
        }

        private void btnGerar_Click(object sender, EventArgs e)
        {
            //Usando construtores
            
            Nota3 nt;
            nt = new Nota3();
            MessageBox.Show("Aluno: " + nt.aluno + "\n" +
                            "Nota 1: " + nt.b1 + "\n" +
                            "Nota 2: " + nt.b2 + "\n" +
                            "Nota 3: " + nt.b3 + "\n" +
                            "Nota 4: " + nt.b4);
            nt = new Nota3(txtAluno.Text);
            MessageBox.Show("Aluno: " + nt.aluno + "\n" +
                            "Nota 1: " + nt.b1 + "\n" +
                            "Nota 2: " + nt.b2 + "\n" +
                            "Nota 3: " + nt.b3 + "\n" +
                            "Nota 4: " + nt.b4);
            nt = new Nota3(txtAluno.Text, float.Parse(txtB1.Text), float.Parse(txtB2.Text), float.Parse(txtB3.Text), float.Parse(txtB4.Text));
            MessageBox.Show("Aluno: " + nt.aluno + "\n" +
                            "Nota 1: " + nt.b1 + "\n" +
                            "Nota 2: " + nt.b2 + "\n" +
                            "Nota 3: " + nt.b3 + "\n" +
                            "Nota 4: " + nt.b4);
        }

        private void txtSituacao_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMedia_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB4_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB3_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtB1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAluno_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
